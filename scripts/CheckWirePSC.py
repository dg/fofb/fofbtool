#!/usr/bin/env python
###################################################################################################
#   Check Wire PSC
###################################################################################################
#
# This script browse through all PSC and check that the cellnode line goes throught the proper PSC
#
###################################################################################################

import tango
import logging
import numpy as np
import time


# Get the module logger
logger = logging.getLogger("CheckWirePSC")

for h in logger.handlers:
    logger.removeHandler(h)

# Add a Stream handler to the module logger
sh=logging.StreamHandler()
sh.setLevel(logging.DEBUG)
sh.setFormatter(logging.Formatter("{levelname:8}: {message}", style='{'))
logger.addHandler(sh)

# Add a file handler
logpath="./CheckWirePSC.log"
try:
    fh=logging.FileHandler(logpath)
except FileNotFoundError:
    logger.warning("Not logging to file, could not open location {}".format(logpath))
else:
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter("{levelname:8}: {message}", style='{'))
    logger.addHandler(fh)

logger.setLevel(logging.DEBUG)

####################################################################################################

logger.debug("Getting Proxies to cellnodes")
prx_cellnode = [tango.DeviceProxy("ans/dg/fofb-cellnode-c{:02d}".format(n)) for n in [1,6,9,14]]

logger.debug("Prepare tango group for PSC")
db = tango.Database()
psc = {k:tango.Group(k+"psc") for k in "hv"}
psclist = {}
for k in "hv":
    psclist[k] = [n for n in db.get_property("FOFB", k+"steererlist")[k+'steererlist']]
    psc[k].add(psclist[k])

logger.info("Check that all lines are running now")
for k in "hv":
    if not np.all([r.get_data().value > 10000 for r in psc[k].read_attribute("nbframesec")]):
        logger.error("Initial test fail, not all PSC are receiving data in plane {}".format(k.upper()))


####################################################################################################

logger.info("Switching all lines off")
for cn in prx_cellnode:
    cn.comcorr_line_id = cn.comcorr_line_id & 0xFF

time.sleep(2)

logger.debug("Check that all lines are not running now")
for k in "hv":
    if np.any([r.get_data().value > 10000 for r in psc[k].read_attribute("nbframesec")]):
        logger.error("Initial test fail, some PSC are receiving data in plane {}, but cellnode has been shut down".format(k.upper()))


####################################################################################################

logger.info("Browsing all PSC lines")

for cn in prx_cellnode:

    for i in range(32):
        logger.debug("Activate line # {:2d} on {}".format(i, cn.name()))

        lid = cn.comcorr_line_id
        pscid = lid[i]
        if pscid in (0, 255):
            continue
        lid[i] = lid[i] + 0x10000
        cn.comcorr_line_id = lid

        time.sleep(2)

        if pscid > 50:
            p='v'
            pscid = pscid - 50
        else:
            p='h'
        pscpath = psclist[p][pscid-1].lower()
        logger.debug("Checking that only #{:02d}-{}  {} is fed".format(pscid, p, pscpath))

        for k in "hv":
            R=psc[k].read_attribute("nbframesec")
            for r in R:
                if r.get_data().value > 100:
                    dn = r.dev_name().lower()
                    if not dn == pscpath:
                        logger.error("{} is receiving data, expecting {}".format(dn, pscpath))

        logger.debug("Deactivate line # {:2d} on {}".format(i, cn.name()))
        lid[i] = lid[i] & 0xFF
        cn.comcorr_line_id = lid
        time.sleep(2)


####################################################################################################

logger.info("Switching all lines on")
for cn in prx_cellnode:
    cn.comcorr_line_id = cn.comcorr_line_id + 0x10000


