#!/usr/bin/env python

from distutils.core import setup
from FofbTool.__init__ import __version__

setup(name='FofbTool',
      version=__version__,
      description='Python module to operate and configure SOLEIL FOFB.',
      url='https://gitlab.synchrotron-soleil.fr/dg/fofb/fofbtool',
      license="GNU-GPL-v3",
      author='Romain BRONÈS',
      author_email='romain.brones@synchrotron-soleil.fr',
      packages=['FofbTool',],
      scripts=['scripts/FofbTool',],
      package_data={
          'FofbTool': ['*.cfg'],
          },
      install_requires = ['pytango>=9.2', 'numpy>=1.15'],
     )
