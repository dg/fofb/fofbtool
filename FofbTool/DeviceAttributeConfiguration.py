###################################################################################################
#    TANGO DB ATTRIBUTE CONFIG
###################################################################################################
#
# This file contains crude functions to configure the attributes of the OPCUAProxy tango devices.
#
###################################################################################################


import logging


# Get the module logger
logger = logging.getLogger("FofbTool")

def set_attr_config_cellnode(cnp):
    """
    Apply CellNode attribute configuration on the Tango Device cnp.
    This does not check the kind of proxy passed. Expect exception if misuse.

    PARAMETERS
    ----------
    cnp: tango.DeviceProxy
        Device Proxy to the target device
    """

    logger.info("Apply CellNode attribute configuration on device {}".format(cnp.name()))

    c = cnp.get_attribute_config("app_version")
    c.description = "FPGA firmware version\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccneth0_gt_reset")
    c.description = "ComCellNode transceiver reset register, write 1 to reset.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.min_value='0'
    c.max_value='7'
    c.alarms.max_warning='1'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccneth0_reset")
    c.description = "ComCellNode data & serdes reset register, write 0xE0000001 to reset.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.min_value='0'
    c.alarms.max_warning='1'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccneth0_rx_framerate")
    c.description = "ComCellNode RX frame rate.\n" +c.description
    c.format = '%d'
    c.unit = 'fps'
    c.alarms.min_warning='10000'
    c.alarms.max_warning='11000'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccneth0_tx_framerate")
    c.description = "ComCellNode TX frame rate.\n" +c.description
    c.format = '%d'
    c.unit = 'fps'
    c.alarms.min_warning='10000'
    c.alarms.max_warning='11000'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccneth0_tx_errorrate")
    c.description = "ComCellNode TX erroneous frame rate.\n" +c.description
    c.format = '%d'
    c.unit = 'fps'
    c.alarms.max_warning='1'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccneth0_gt_status")
    c.description = "ComCellNode ethernet status. Expected value 0x70 or 0x71\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.alarms.min_warning=str(0x6f)
    c.alarms.max_warning=str(0x72)
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnpack0_npackets")
    c.description = "ComCellNode number of packet to send -1. Usually 31 for C14 and 29 for others.\n" +c.description
    c.format = '%d'
    c.unit = 'packets'
    c.min_value='0'
    c.max_value='999'
    c.alarms.min_warning='28'
    c.alarms.max_warning='32'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnpack0_control")
    c.description = "ComCellNode packeter control, write 1 to start.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.min_value='0'
    c.max_value='2'
    c.alarms.min_warning='0'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnpack0_framesize")
    c.description = "ComCellNode output frame size. 330 for C14 else 310.\n" +c.description
    c.format = '%d'
    c.unit = 'bytes'
    c.min_value='0'
    c.max_value='999'
    c.alarms.min_warning='309'
    c.alarms.max_warning='331'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnpack0_error")
    c.description = "ComCellNode packeter protocol error flags.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.alarms.max_warning='1'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnpack0_framecount")
    c.description = "ComCellNode number of frame sent.\n" +c.description
    c.format = '%d'
    c.unit = 'frames'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnunpack0_framecount")
    c.description = "ComCellNode number of frame received.\n" +c.description
    c.format = '%d'
    c.unit = 'frames'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnunpack0_control")
    c.description = "ComCellNode unpacketer control, write 1 to start.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.min_value='0'
    c.max_value='1'
    c.alarms.min_warning='0'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnunpack0_error")
    c.description = "ComCellNode unpacketer protocol error flags.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.alarms.max_warning='1'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("ccnunpack0_framesize")
    c.description = "ComCellNode input frame size. Usual value = 610.\n" +c.description
    c.format = '%d'
    c.unit = 'bytes'
    c.min_value='0'
    c.max_value='999'
    c.alarms.min_warning='609'
    c.alarms.max_warning='611'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("combpm_protocol_status")
    c.description = "ComCellNode ethernet status. Expected value 0x00\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.alarms.max_warning='4'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("combpm_sfp")
    c.description = "ComBPM SFP status. Expected value 0x00\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.alarms.max_warning='1'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("combpm_gt_status")
    c.description = "ComBPM transceiver status.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("combpm_valid_framerate")
    c.description = "ComBPM RX frame rate.\n" +c.description
    c.format = '%d'
    c.unit = 'fps'
    c.alarms.min_warning='1220000'
    c.alarms.max_warning='1230000'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("combpm_invalid_framecount")
    c.description = "ComBPM RX invalid frame rate. Should not increase to quickly (<1/s)\n" +c.description
    c.format = '%d'
    c.unit = 'frames'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("combpm_frameseq")
    c.description = "ComBPM number of frames received -1 in last sequence.\n" +c.description
    c.format = '%d'
    c.unit = 'frames'
    c.alarms.min_warning='121'
    c.alarms.max_warning='123'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("combpm_reset")
    c.description = "ComBPM protocol reset register, write 1 to reset.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.min_value='0'
    c.max_value='1'
    c.alarms.max_warning='1'
    cnp.set_attribute_config(c)

    c = cnp.get_attribute_config("combpm_gt_control")
    c.description = "ComBPM transceiver reset register, 1 to enable, 5 to reset.\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    c.min_value='0'
    c.max_value='7'
    c.alarms.min_warning='0'
    c.alarms.max_warning='2'
    cnp.set_attribute_config(c)

def set_attr_config_centralnode(cnp):
    """
    Apply Centralnode attribute configuration on the Tango Device cnp.
    This does not check the kind of proxy passed. Expect exception if misuse.

    PARAMETERS
    ----------
    cnp: tango.DeviceProxy
        Device Proxy to the target device
    """


    logger.info("Apply CentralNode attribute configuration on device {}".format(cnp.name()))

    c = cnp.get_attribute_config("app_version")
    c.description = "FPGA firmware version\n" +c.description
    c.format = '0x%X'
    c.unit = ' '
    cnp.set_attribute_config(c)

    for n in range(4):
        c = cnp.get_attribute_config("ccneth{}_gt_reset".format(n))
        c.description = "ComCellNode transceiver reset register, write 1 to reset.\n" +c.description
        c.format = '0x%X'
        c.unit = ' '
        c.min_value='0'
        c.max_value='7'
        c.alarms.max_warning='1'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccneth{}_reset".format(n))
        c.description = "ComCellNode data & serdes reset register, write 0xE0000001 to reset.\n" +c.description
        c.format = '0x%X'
        c.unit = ' '
        c.min_value='0'
        c.alarms.max_warning='1'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccneth{}_rx_framerate".format(n))
        c.description = "ComCellNode RX frame rate.\n" +c.description
        c.format = '%d'
        c.unit = 'fps'
        c.alarms.min_warning='10000'
        c.alarms.max_warning='11000'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccneth{}_tx_framerate".format(n))
        c.description = "ComCellNode TX frame rate.\n" +c.description
        c.format = '%d'
        c.unit = 'fps'
        c.alarms.min_warning='10000'
        c.alarms.max_warning='11000'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccneth{}_tx_errorrate".format(n))
        c.description = "ComCellNode TX erroneous frame rate.\n" +c.description
        c.format = '%d'
        c.unit = 'fps'
        c.alarms.max_warning='1'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccneth{}_gt_status".format(n))
        c.description = "ComCellNode ethernet status. Expected value 0x70 or 0x71\n" +c.description
        c.format = '0x%X'
        c.unit = ' '
        c.alarms.min_warning=str(0x6f)
        c.alarms.max_warning=str(0x72)
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnpack{}_npackets".format(n))
        c.description = "ComCellNode number of packet to send -1. Usual value 99.\n" +c.description
        c.format = '%d'
        c.unit = 'packets'
        c.min_value='0'
        c.max_value='999'
        c.alarms.min_warning='98'
        c.alarms.max_warning='100'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnpack{}_control".format(n))
        c.description = "ComCellNode packeter control, write 1 to start.\n" +c.description
        c.format = '0x%X'
        c.unit = ' '
        c.min_value='0'
        c.max_value='2'
        c.alarms.min_warning='0'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnpack{}_framesize".format(n))
        c.description = "ComCellNode output frame size. Usual value 610.\n" +c.description
        c.format = '%d'
        c.unit = 'bytes'
        c.min_value='0'
        c.max_value='999'
        c.alarms.min_warning='609'
        c.alarms.max_warning='611'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnpack{}_error".format(n))
        c.description = "ComCellNode packeter protocol error flags.\n" +c.description
        c.format = '0x%X'
        c.unit = ' '
        c.alarms.max_warning='1'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnpack{}_framecount".format(n))
        c.description = "ComCellNode number of frame sent.\n" +c.description
        c.format = '%d'
        c.unit = 'frames'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnunpack{}_framecount".format(n))
        c.description = "ComCellNode number of frame received.\n" +c.description
        c.format = '%d'
        c.unit = 'frames'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnunpack{}_control".format(n))
        c.description = "ComCellNode unpacketer control, write 1 to start.\n" +c.description
        c.format = '0x%X'
        c.unit = ' '
        c.min_value='0'
        c.max_value='1'
        c.alarms.min_warning='0'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnunpack{}_error".format(n))
        c.description = "ComCellNode unpacketer protocol error flags.\n" +c.description
        c.format = '0x%X'
        c.unit = ' '
        c.alarms.max_warning='1'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("ccnunpack{}_framesize".format(n))
        c.description = "ComCellNode input frame size. 330 from C14 else 310.\n" +c.description
        c.format = '%d'
        c.unit = 'bytes'
        c.min_value='0'
        c.max_value='999'
        c.alarms.min_warning='309'
        c.alarms.max_warning='331'
        cnp.set_attribute_config(c)


    c = cnp.get_attribute_config("corr_num_bpm")
    c.description = "Number of expected bpm packets for the matrix multiplication."+c.description
    c.format = '%d'
    c.unit = ' '
    c.alarms.min_warning='121'
    c.alarms.max_warning='123'
    cnp.set_attribute_config(c)

    for p in "xy":

        c = cnp.get_attribute_config("corr_control_{}".format(p))
        c.description = "Correction controller for {} plan. 1 is feedback running, 2 is reset correctors.\n".format(p.upper()) +c.description
        c.format = '0x%X'
        c.unit = ' '
        c.min_value='0'
        c.max_value='2'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("corr_command_{}".format(p))
        c.description = "Correction command sent for {} plan, in Ampers. Mobile mean on 1 second.\n".format(p.upper()) +c.description
        c.format = '%f'
        c.unit = 'A'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("orbit_error_{}".format(p))
        c.description = "Orbit error read from BPMs, in nm. Mobile mean on 1 second.\n".format(p.upper()) +c.description
        c.format = '%d'
        c.unit = 'nm'
        cnp.set_attribute_config(c)

        c = cnp.get_attribute_config("corr_ref_{}".format(p))
        c.description = "Orbit reference, in nm.\n".format(p.upper()) +c.description
        c.format = '%d'
        c.unit = 'nm'
        cnp.set_attribute_config(c)

        for n in "12":
            for cf in ("a","b","ic","d"):
                c = cnp.get_attribute_config("corr_k{}{}_{}".format(n,cf,p))
                c.description = "Correction controller coefficient {}, filter {} plan\n".format(n,cf,p) +c.description
                c.format = '%d'
                c.unit = ' '
                cnp.set_attribute_config(c)

