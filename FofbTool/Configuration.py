###################################################################################################
#    CONFIGURATION FUNCTIONS
###################################################################################################
#
# Contains functions to configure blocks of the FOFB application.
#
###################################################################################################

import tango
import logging
import numpy as np
import FofbTool.Utils


# Get the module logger
logger = logging.getLogger("FofbTool")

###################################################################################################
#    CONFIGURE COM BPM
###################################################################################################

def cellnode_configure_combpm(node_tangopath, bpmallowed):
    """
    Configure the combpm block of a CellNode.
    The BPM data allowed through are taken in the argument.

    PARAMETERS
    ----------
    node_tangopath: str
        The target fofbnode tango path, ie 'ans/dg/fofb-cellnode-c09'
    bpmallowed: [int], None
        List of BPMID allowed through the block.

    RETURN
    ------
    success: boolean
        True if configuration is a success
    """

    # Get device proxy
    try:
        prx=tango.DeviceProxy(node_tangopath)
        prx.ping()
    except tango.DevFailed:
        logger.error("Failed to obtain tango proxy or to ping to {}".format(node_tangopath))
        return None

    logger.debug("Activate BPMs {} in the filter for {}".format(bpmallowed, node_tangopath))
    f = prx["combpm_filter_table"].value
    f[:] = 0
    f[np.array(bpmallowed)] = 0x80
    prx["combpm_filter_table"] = f

    logger.info("Configuration of ComBpm done on {}.".format(node_tangopath))
    return True

###################################################################################################
#    CONFIGURE COM LBP
###################################################################################################

def cellnode_configure_comlbp(node_tangopath, seqoffset=0):
    """
    Configure the comlbp block of a CellNode.
    For now, nothing done

    PARAMETERS
    ----------
    node_tangopath: str
        The target fofbnode tango path, ie 'ans/dg/fofb-cellnode-c09'

    seqoffset: int
        Sequence offset to configure

    RETURN
    ------
    success: boolean
        True if configuration is a success
    """

    # Get device proxy
    try:
        prx=tango.DeviceProxy(node_tangopath)
        prx.ping()
    except tango.DevFailed:
        logger.error("Failed to obtain tango proxy or to ping to {}".format(node_tangopath))
        return None

    logger.debug("Program sequence offset {} in {}".format(seqoffset, node_tangopath))
    for n in range(4):
        prx["comlbp{}_seqoffset".format(n)]=seqoffset

    logger.info("Configuration of ComLBP done on {}.".format(node_tangopath))
    return True

###################################################################################################
#    CONFIGURE CCN: COM CELLNODES
###################################################################################################


def cellnode_configure_ccn(node_tangopath, nbpm, npsc):
    """
    Configure the ComCellNode block on a cellnode.
    Automatically set the number of bpm/psc packets and MAC length.

    PARAMETERS
    ----------
    node_tangopath: str
        The target fofbnode tango path, ie 'ans/dg/fofb-cellnode-c09'
    nbpm:
        Number of BPM allowed by the filter, hence the number of expected BPM packets.
    npsc:
        Number of total PSC, hence the number of expected PSC packets.

    RETURN
    ------
    success: boolean
        True if configuration is a success
    """

    # Get device proxy
    try:
        prx=tango.DeviceProxy(node_tangopath)
        prx.ping()
    except tango.DevFailed:
        logger.error("Failed to obtain tango proxy or to ping to {}".format(node_tangopath))
        return None

    logger.debug("{} bpm allowed in the ethernet frame on {}".format(nbpm, node_tangopath))
    logger.debug("{} psc expected in the ethernet frame on {}".format(nbpm, node_tangopath))

    maclen = 10*nbpm+10
    logger.debug("Configure packeter framesize (mac length) to {}".format(maclen))
    prx["ccnpack0_framesize"] = maclen

    logger.debug("Configure packeter npackets to {}".format(nbpm-1))
    prx["ccnpack0_npackets"] = nbpm-1

    maclen = npsc*6+10
    logger.debug("Configure unpacketer framesize (mac length) to {}".format(maclen))
    prx["ccnunpack0_framesize"] = maclen

    logger.info("Configuration of CCN done on {}.".format(node_tangopath))
    return True

def centralnode_configure_ccn(node_tangopath, nbpm, npsc):
    """
    Configure the ComCellNode block on the centralnode.
    Automatically set the number of bpm/psc packets and MAC length.

    PARAMETERS
    ----------
    node_tangopath: str
        The target fofbnode tango path, ie 'ans/dg/fofb-centralnode'
    nbpm: list(int)
        Number of BPM packet received on each interface.
    npsc:
        Number of total PSC, hence the number of expected PSC packets.

    RETURN
    ------
    success: boolean
        True if configuration is a success
    """
    # Get device proxy
    try:
        prx=tango.DeviceProxy(node_tangopath)
        prx.ping()
    except tango.DevFailed:
        logger.error("Failed to obtain tango proxy or to ping to {}".format(node_tangopath))
        return None

    for n in range(len(nbpm)):
        maclen = npsc*6+10
        logger.debug("Configure packeter {} framesize (mac length) to {}".format(n, maclen))
        prx["ccnpack{}_framesize".format(n)] = maclen

        logger.debug("Configure packeter {}  npackets to {}".format(n, npsc-1))
        prx["ccnpack{}_npackets".format(n)] = npsc-1

        maclen = 10*nbpm[n]+10
        logger.debug("Configure unpacketer {} framesize (mac length) to {}".format(n, maclen))
        prx["ccnunpack{}_framesize".format(n)] = maclen

    logger.info("Configuration of CCN done on {}.".format(node_tangopath))
    return True

###################################################################################################
#    CONFIGURE COM CORR
###################################################################################################

def cellnode_configure_comcorr(node_tangopath, pscid, enable):
    """
    Configure the comcorr block of a CellNode.
    The PSC ID either taken in the argument, or default to nominal values.


    PARAMETERS
    ----------
    node_tangopath: str
        The target fofbnode tango path, ie 'ans/dg/fofb-cellnode-c09'
    pscid: [int], None
        List of 32 PSCID to program on the output board. If None, default to nominal values.

    enable: boolean
        Enable or not the hardware outputs.

    RETURN
    ------
    success: boolean
        True if configuration is a success
    """
    # Get device proxy
    try:
        prx=tango.DeviceProxy(node_tangopath)
        prx.ping()
    except tango.DevFailed:
        logger.error("Failed to obtain tango proxy or to ping to {}".format(node_tangopath))
        return None

    logger.debug("Set PSCIDs {} in the filter for {}".format(pscid, node_tangopath))
    f= prx["comcorr_line_id"].value
    f[:] = pscid
    f[:] = f+ 0x10000*bool(enable)
    logger.debug("Set comcorr_line_id {} for {}".format(f, node_tangopath))
    prx["comcorr_line_id"] = f

    logger.info("Configuration of ComCorr done on {}.".format(node_tangopath))
    return True

###################################################################################################
#    CONFIGURE MATRIX CORRECTOR
###################################################################################################

def centralnode_configure_corr(node_tangopath, numbpm, pscid, k1x, k1y, k2x, k2y):
    """
    Configure the correction algorithm on the centralnode.

    PARAMETERS
    ----------
    node_tangopath: str
        The target fofbnode tango path, ie 'ans/dg/fofb-centralnode'
    k1x, k1y, k2x, k2y: list(int)
        List of coefficient for each filter. 4 coefficients : ABCD
    """

    # Get device proxy
    try:
        prx=tango.DeviceProxy(node_tangopath)
        prx.ping()
    except tango.DevFailed:
        logger.error("Failed to obtain tango proxy or to ping to {}".format(node_tangopath))
        return None

    for i,l in enumerate(["a","b","ic","d"]):
        prx["corr_k1{}_x".format(l)]=k1x[i]
        prx["corr_k2{}_x".format(l)]=k2x[i]
        prx["corr_k1{}_y".format(l)]=k1y[i]
        prx["corr_k2{}_y".format(l)]=k2y[i]

    f= prx["corr_pscid"].value
    f[0:100] = pscid
    prx["corr_pscid"] = f

    prx["corr_num_bpm"] = numbpm

    logger.info("Configuration of Corr done on {}.".format(node_tangopath))
    return True

