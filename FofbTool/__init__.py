"""
FofbTool is a module to operate and configure the Fast Orbit Feedback (FOFB) platform at SOLEIL.

This package is cut in several modules:
    * FofbTool.Utils
        Contains usefull functions, or first level function that make use of the core functions.

    * FofbTool.Operation
        Contains core functions to make actions on the (FOFB) platform.

    * FofbTool.Configuration
        Contains core functions to apply configurations on the FOFB platform

For basic usage, see doc of FofbTool.Utils

"""
__version__ = "3.2"
